module restapiswithcouchdb

go 1.13

require (
	github.com/go-kivik/couchdb/v4 v4.0.0-20210606123901-7c3aefbfa34c
	github.com/go-kivik/kivik v2.0.0+incompatible
	github.com/go-kivik/kivik/v4 v4.0.0-20210606210530-2cf07af3502e
	github.com/gorilla/mux v1.8.0
)
