Database Connection:
- http://admin:admin@localhost:5984/student 

Application Run on Port 8000

Bulk insert into the database with the given File or API.
- http://localhost:8000/student (GET)

Find a document by id and reverse number
- http://localhost:8000/student/id?rev=rev (GET)

Update & Delete Document by id and revision number
- Update http://localhost:8000/update_student/id?rev=rev (PUT)
- Delete http://localhost:8000/delete_student/id?rev=rev (DELETE)

Design document with MapReduce or Mango Query to filter student by Classroom’s Name.
- http://localhost:8000/filter_student?class=class (GET)

Get All Students
- http://localhost:8000/students (GET)

Design Document ID "_design/student-info" 
Views Name "filterbyclassname"
Design document function
function(doc, req){
emit(doc.classname, doc);
}
url: http://localhost:5984/student/_design/student-info/_view/filterbyclassname?key=%22SR%22

Why do need to have a REST API for an existing API of CouchDB?

There are several reason why do need to have a REST APIs for an existing API of CouchDB:

- non JSON resources (we cannot insert non JSON resource to CouchDB APIs so we need to create APIs to convert it to JSON)
- custom backend logic (sometime we need to do some logic in backend before accessing to CouchDB REST APIs)
- authentication (the auth options for Couch are limited)
- server-side filtering (might be easier with custom APIs than an update handler in Couch)
- security (Couldn't access directly to CouchDB)
