package student

type Student struct {
	StudentId       int    `json:"studentid"`
	StudentName     string `json:"studentname"`
	Gender          string `json:"gender"`
	Info            string `json:"info"`
	IsClassMonintor bool   `json:"is_class_monitor"`
	ClassName       string `json:"classname"`
	TeamName        string `json:"teamname"`
}

type StudentReq struct {
	Rev             string `json:"_rev"`
	StudentId       int    `json:"studentid"`
	StudentName     string `json:"studentname"`
	Gender          string `json:"gender"`
	Info            string `json:"info"`
	IsClassMonintor bool   `json:"is_class_monitor"`
	ClassName       string `json:"classname"`
	TeamName        string `json:"teamname"`
}

type ResponseObject struct {
	Record []struct {
		StudentId       int    `json:"studentid"`
		StudentName     string `json:"studentname"`
		Gender          string `json:"gender"`
		Info            string `json:"info"`
		IsClassMonintor bool   `json:"is_class_monitor"`
		ClassName       string `json:"classname"`
		TeamName        string `json:"teamname"`
	} `json:"record"`
}

type JsonResponse struct {
	Success bool      `json:"success"`
	Data    []Student `json:"data"`
	Message string    `json:"message"`
}
