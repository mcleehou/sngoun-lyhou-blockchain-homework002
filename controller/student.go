package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	. "restapiswithcouchdb/db"
	. "restapiswithcouchdb/student"

	_ "github.com/go-kivik/couchdb/v4"
	kivik "github.com/go-kivik/kivik/v4"
	"github.com/gorilla/mux"
)

///Insert Student
func InsertStudent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	//connect to database
	client, err := kivik.New("couch", Dburl)
	if err != nil {
		panic(err)
	}
	db := client.DB("student")

	res, err := http.NewRequest(http.MethodGet, "https://api.jsonbin.io/v3/b/6166ea0baa02be1d4458d5f6", nil)
	data, _ := http.DefaultClient.Do(res)
	body, _ := ioutil.ReadAll(data.Body)

	var student2 ResponseObject
	json.Unmarshal(body, &student2)
	for _, v := range student2.Record {
		_, _, err = db.CreateDoc(context.TODO(), v)
	}
	// db.BulkDocs(context.TODO(), student2.Record)
	var response = JsonResponse{Success: true, Message: "Student has been Inserted successfully!"}
	json.NewEncoder(w).Encode(response)
}

// GetStudentByID and rev
func GetStudentByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	Id := params["id"]
	rev := params["rev"]
	fmt.Println("Rev:", rev)
	//connect to database
	client, err := kivik.New("couch", Dburl)
	if err != nil {
		panic(err)
	}
	db := client.DB("student")
	rows := db.Get(context.TODO(), Id, kivik.Options{
		"_rev": rev,
	})
	var student []Student
	var temp Student
	if err = rows.ScanDoc(&temp); err != nil {
		panic(err)
	}
	student = append(student, temp)
	var response = JsonResponse{Success: true, Data: student, Message: "Student has been fetched successfully!"}
	json.NewEncoder(w).Encode(response)
}

////Get All Students
func GetAllStudents(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	//connect to database
	client, err := kivik.New("couch", Dburl)
	if err != nil {
		panic(err)
	}
	db := client.DB("student")
	rows, err := db.Query(context.TODO(), "_design/student-info", "_view/filterbyclassname")
	if err != nil {
		panic(err)
	}
	var temp []Student
	for rows.Next() {
		var doc Student
		rows.ScanValue(&doc)
		temp = append(temp, doc)
	}
	if rows.Err() != nil {
		panic(rows.Err())
	}
	var response = JsonResponse{Success: true, Data: temp, Message: "All Student has been fetched successfully!"}
	json.NewEncoder(w).Encode(response)
}

///Update Student by ID and rev
func UpdateStudentByIDAndRev(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	Id := params["id"]
	rev := params["rev"]
	var studentReq StudentReq
	_ = json.NewDecoder(r.Body).Decode(&studentReq)
	studentReq.Rev = rev

	//connect to database
	client, err := kivik.New("couch", Dburl)
	if err != nil {
		panic(err)
	}
	db := client.DB("student")
	_, err = db.Put(context.TODO(), Id, studentReq)
	if err != nil {
		panic(err)
	}
	var response = JsonResponse{Success: true, Message: "Student has been Updated successfully!"}
	json.NewEncoder(w).Encode(response)
}

///Filter student by classname ///TODO not yet get data from request
func FilterStudentByClassName(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	class := params["class"]
	//connect to database
	client, err := kivik.New("couch", Dburl)
	if err != nil {
		panic(err)
	}
	db := client.DB("student")
	rows, err := db.Query(context.TODO(), "_design/student-info", "_view/filterbyclassname", kivik.Options{"key": class})
	if err != nil {
		panic(err)
	}
	var temp []Student
	for rows.Next() {
		var doc Student
		rows.ScanValue(&doc)
		temp = append(temp, doc)
	}
	if rows.Err() != nil {
		panic(rows.Err())
	}
	var response = JsonResponse{Success: true, Data: temp, Message: "Filtered Student has been fetched successfully!"}
	json.NewEncoder(w).Encode(response)
}

///Delete document by ID and rev //Done
func DeleteStudentByIDAndRev(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	Id := params["id"]
	rev := params["rev"]
	//connect to database
	client, err := kivik.New("couch", Dburl)
	if err != nil {
		log.Fatal(err)
	}
	db := client.DB("student")
	_, err = db.Delete(context.TODO(), Id, rev)
	if err != nil {
		log.Fatal(err)
	}
	var response = JsonResponse{Success: true, Message: "Student has been Deleted successfully!"}
	json.NewEncoder(w).Encode(response)
}
