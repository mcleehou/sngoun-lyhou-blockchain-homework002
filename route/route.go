package route

import (
	"fmt"
	"log"
	"net/http"
	. "restapiswithcouchdb/controller"

	"github.com/gorilla/mux"
)

func Router() {
	router := mux.NewRouter()

	// Route handles & endpoints

	// Get Student By ID
	router.Path("/student/{id}").Queries("rev", "{rev}").HandlerFunc(GetStudentByID)

	// Create a Student
	router.HandleFunc("/student", InsertStudent).Methods("GET")

	// Get All Students
	router.HandleFunc("/students", GetAllStudents).Methods("GET")

	//// Delete Student by id and rev
	router.Path("/delete_student/{id}").Queries("rev", "{rev}").HandlerFunc(DeleteStudentByIDAndRev).Methods("DELETE")

	//// Update Student by id and rev
	router.Path("/update_student/{id}").Queries("rev", "{rev}").HandlerFunc(UpdateStudentByIDAndRev).Methods("PUT")

	//// Update Student by id and rev
	router.Path("/filter_student").Queries("class", "{class}").HandlerFunc(FilterStudentByClassName)

	// serve the app
	fmt.Println("Server at 8000")
	log.Fatal(http.ListenAndServe(":8000", router))

}
